import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-hours',
  templateUrl: './hours.component.html',
  styleUrls: ['./hours.component.scss']
})
export class HoursComponent implements OnInit {
  myvalue : boolean;
  constructor() { }
  ngOnInit() {
  }

  changeClass = () =>{
    this.myvalue = true;
    return this.myvalue;
  }

}
