import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {Routes, RouterModule} from '@angular/router';
import {LocationComponent} from '../app/location/location.component';
import { HoursComponent } from './hours/hours.component';


const routes : Routes = [
  {
    path : '',
    component : LocationComponent
  },
  {
    path : 'page2',
    component : HoursComponent
  }
]

@NgModule({
  imports: [ RouterModule.forRoot(routes)],
  exports : [RouterModule],
  declarations: []
})
export class AppRoutingModule { }
