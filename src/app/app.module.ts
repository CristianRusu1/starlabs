import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { AppRoutingModule } from './/app-routing.module';
import { LocationComponent } from './location/location.component';
import { HoursComponent } from './hours/hours.component';


@NgModule({
  declarations: [
    AppComponent,
    LocationComponent,
    HoursComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
